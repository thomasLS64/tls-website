// Environment Variable
const HTTP_PORT = 9999;



// Local Dependencies
const Log = require('../libs/log');

// Extern Dependencies
const MongoClient = require('mongodb').MongoClient,
      ObjectId = require('mongodb').ObjectID,
      bodyParser = require('body-parser'),
      express = require('express'),
      http = require('http'),
      app = express(),
      crypto = require('crypto');


// Connection to MongoDB
const MongoDB = MongoClient.connect('mongodb://localhost:27017');


// Create & Bind Server
var httpServer = http.createServer(app);

var httpListener = httpServer.listen(HTTP_PORT, () => {
  var addr = httpListener.address();

  Log.info('HTTPServer', `Server is listening on ${addr.address}:${addr.port}`);
});



// Middleware
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



/**
 *  ------------------------
 *  --      API REST      --
 *  ------------------------
**/
function generateRandomToken(cst) {
  let date = new Date(),
      binaryRand = '1';
  
  for (let i=0; i<1000; i++) {
    binaryRand += Math.round(Math.random());
  }

  return crypto
            .createHash('sha1')
            .update(`${cst}_${parseInt(binaryRand, 2)}_${date.getTime()}`)
            .digest('hex');
}

/**
 *  [REST - GET] /user
 *  Get users
**/
app.get('/user', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('user'),
          requestType = req.query.type;

    delete req.query.type;

    collection.find(req.query).toArray(function(err, users) {
      if (!err) {
        if (requestType && 'connexion' === requestType) {
          let user = users[0];

          if (user) {
            delete user.password;

            res.json({ 
              status: 200,
              token: generateRandomToken(user._id),
              user: user
            })

            // TODO - Save Token
          } else {
            res.json({
              status: 404,
              message: 'User not found'
            })
          }
        } else {
          users.forEach(elt => {
            delete elt.password
          })
          res.json(users)
        }
      } else {
        res.send(undefined)
      }
    })
  });
})

/**
 *  [REST - POST] /user
 *  Save a new user
**/
app.post('/user', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('user');

    let user = {
      username: req.body.login,
      password: req.body.password
    }

    collection.insertOne(user, function(err, data) {
      if (!err && data.insertedCount === 1) {
        res.json(data.ops[0])
      } else {
        res.send(undefined)
      }
    })
  });
})


/**
 *  [REST - GET] /timeline
 *  Get all timeline steps
**/
app.get('/timeline', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('timeline');

    collection.find({}).toArray(function(err, docs) {
      res.send(docs);
    })
  });
})

/**
 *  [REST - GET] /timeline/:id
 *  Get all details of particular timeline step
**/
app.get('/timeline/:id', function(req, res) {
  if (req.params.id) {
    MongoDB.then(function(client) {
      const db = client.db('tls-site'),
            collection = db.collection('timeline');

      collection.findOne({ _id: new ObjectId(req.params.id) }, function(err, doc) {
        if (!err) {
          res.send(doc);
        } else {
          res.send(undefined);
        }
      })
    });
  } else {
    res.send(undefined);
  }
})



/**
 *  [REST - GET] /article
 *  Get all articles
**/
app.get('/article', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('article');

    collection.find({}).toArray(function(err, docs) {
      res.json(docs);
    })
  });
})

/**
 *  [REST - POST] /article
 *  Save a new article
**/
app.post('/article', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('article');

    collection.insertOne(req.body, function(err, data) {
      if (!err && data.insertedCount === 1) {
        res.json(data.ops[0])
      } else {
        res.send(undefined)
      }
    })
  });
})

/**
 *  [REST - PUT] /article/:id
 *  Update an article define by the ID
**/
app.put('/article/:id', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('article');

    collection.updateOne({ _id: new ObjectId(req.params.id) }, { $set: req.body }, function(err, data) {
      if (!err && data.modifiedCount === 1) {
        let article = req.body;

        article._id = req.params.id;
        res.json(article)
      } else {
        res.send(undefined)
      }
    })
  });
})

/**
 *  [REST - DELETE] /article/:id
 *  Remove an article define by the ID
**/
app.delete('/article/:id', function(req, res) {
  MongoDB.then(function(client) {
    const db = client.db('tls-site'),
          collection = db.collection('article');

    collection.deleteOne({ _id: new ObjectId(req.params.id) }, function(err, data) {
      if (!err && data.deletedCount === 1) {
        res.json({ _id: req.params.id })
      } else {
        res.send(undefined)
      }
    })
  });
})

/**
 *  [REST - GET] /article/:id
 *  Get all detail of particular article
**/
app.get('/article/:id', function(req, res) {
  if (req.params.id) {
    MongoDB.then(function(client) {
      const db = client.db('tls-site'),
            collection = db.collection('article');

      collection.findOne({ _id: new ObjectId(req.params.id) }, function(err, doc) {
        if (!err) {
          res.json(doc);
        } else {
          res.send(undefined);
        }
      })
    });
  } else {
    res.send(undefined);
  }
})