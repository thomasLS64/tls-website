export default class Helpers {

  static formatDate(d) {
    let strDate = 'Non défini';
    
    if (d) {
      let date = new Date(d);

      strDate = date.getDate() + ' ';

      switch (date.getMonth()) {
        case 0:
          strDate += 'Janvier'; break;
        case 1:
          strDate += 'Février'; break;
        case 2:
          strDate += 'Mars'; break;
        case 3:
          strDate += 'Avril'; break;
        case 4:
          strDate += 'Mai'; break;
        case 5:
          strDate += 'Juin'; break;
        case 6:
          strDate += 'Juillet'; break;
        case 7:
          strDate += 'Août'; break;
        case 8:
          strDate += 'Septembre'; break;
        case 9:
          strDate += 'Octobre'; break;
        case 10:
          strDate += 'Novembre'; break;
        case 11:
          strDate += 'Decembre';
      }

      strDate += ' ' + date.getFullYear();
    }

    return strDate;
  }


  static obj2query(obj) {
    return '?' + Object.keys(obj).map(elt => `${elt}=${obj[elt]}`).join('&');
  }

}