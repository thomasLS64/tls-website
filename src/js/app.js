// Local lib
import Log from '../../libs/log.js';

// External lib
import Vue from 'vue/dist/vue.js';
import VueRouter from 'vue-router/dist/vue-router.js';

// Import Pages
import HomePage from './pages/home.vue';
import BlogPage from './pages/blog.vue';
import TimelinePage from './pages/timeline.vue';
import ArticlePage from './pages/article.vue';
import WriteArticlePage from './pages/write-article.vue';

// Import Components
import HeaderComponent from './components/header.vue';
import MenuComponent from './components/menu.vue';


Vue.use(VueRouter)

const app = new Vue({
  el: '#app',
  components: {
    'app-header': HeaderComponent,
    'toolbar-menu': MenuComponent
  },
  router: new VueRouter({
    routes: [
      { path: '/',                    name: 'home',             component: HomePage           },
      { path: '/blog',                name: 'blog',             component: BlogPage           },
      { path: '/timeline',            name: 'timeline',         component: TimelinePage       },
      { path: '/article/write',       name: 'write-article',    component: WriteArticlePage   },
      { path: '/article/update/:id',  name: 'update-article',   component: WriteArticlePage   },
      { path: '/article/:id',         name: 'article',          component: ArticlePage        }
    ]
  })
})
