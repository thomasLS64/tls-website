import Ajax from 'ajax-request';
import MongoRESTConf from './_configuration';
import Helpers from '../tools/Helpers';


export default class User {

  static get(params, fn) {
    let url = MongoRESTConf.url + '/user' + Helpers.obj2query(params);

    Ajax(url, function(err, res, body) {
      if(res.statusCode === 200) {
        fn(undefined, JSON.parse(body))
      } else {
        fn(err, undefined)
      }
    })
  }

  static add(user, fn) {
    Ajax.post({
        url: MongoRESTConf.url + '/user',
        data: user
      }, function(err, res, body) {
        if(res.statusCode === 200) {
          fn(undefined, JSON.parse(body))
        } else {
          fn(err, undefined)
        }
      })
  }

}