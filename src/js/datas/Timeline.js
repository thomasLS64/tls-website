import Ajax from 'ajax-request';
import MongoRESTConf from './_configuration';


export default class Timeline {

  static get(id, fn) {
    let url = MongoRESTConf.url + '/timeline' + (id ? `/${id}` : '');

    Ajax(url, function(err, res, body) {
      if(res.statusCode === 200) {
        fn(undefined, JSON.parse(body))
      } else {
        fn(err, undefined)
      }
    })
  }

}