import Ajax from 'ajax-request';
import MongoRESTConf from './_configuration';


export default class Article {

  static get(id, fn) {
    let url = MongoRESTConf.url + '/article' + (id ? `/${id}` : '');

    Ajax(url, function(err, res, body) {
      if(res.statusCode === 200) {
        fn(undefined, JSON.parse(body))
      } else {
        fn(err, undefined)
      }
    })
  }

  static add(article, fn) {
    Ajax.post({
        url: MongoRESTConf.url + '/article',
        data: article
      }, function(err, res, body) {
        if(res.statusCode === 200) {
          fn(undefined, JSON.parse(body))
        } else {
          fn(err, undefined)
        }
      })
  }

  static update(id, article, fn) {
    Ajax({
        method: 'PUT',
        url: MongoRESTConf.url + '/article/' + id,
        data: article
      }, function(err, res, body) {
        if(res.statusCode === 200) {
          fn(undefined, JSON.parse(body))
        } else {
          fn(err, undefined)
        }
      })
  }

  static delete(id, fn) {
    Ajax({
        method: 'DELETE',
        url: MongoRESTConf.url + '/article/' + id
      }, function(err, res, body) {
        if(res.statusCode === 200) {
          fn(undefined, JSON.parse(body))
        } else {
          fn(err, undefined)
        }
      })
  }

}